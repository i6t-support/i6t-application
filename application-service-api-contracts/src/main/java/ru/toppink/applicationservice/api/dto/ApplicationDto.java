package ru.toppink.applicationservice.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.toppink.applicationservice.api.enums.ApplicationReason;
import ru.toppink.applicationservice.api.enums.ApplicationStatus;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ApplicationDto {

    private UUID id;
    private String lastNameClient;
    private String firstNameClient;
    private String patronymicClient;
    private String phoneNumber;
    private String contractNumber;
    private OffsetDateTime createdAt;
    private OffsetDateTime updatedAt;
    private String address;
    private String appealText;
    private ApplicationStatus applicationStatus;
    private String operator;
    private List<CommentDto> comments;
    private String executor;
    private ApplicationReason reason;
}
