package ru.toppink.applicationservice.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.toppink.applicationservice.api.enums.ApplicationReason;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreatedApplicationDto {

    private String loginClient;
    private String lastNameClient;
    private String firstNameClient;
    private String patronymicClient;
    private String phoneNumber;
    private String contractNumber;
    private String address;
    private String appealText;
    private ApplicationReason reason;
}
