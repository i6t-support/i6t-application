package ru.toppink.applicationservice.api.enums;

public enum  ApplicationReason {

    NOT_WORK_INTERNET,
    QUESTION,
    CHANGE_TARIFF,
    OTHER;
}
