package ru.toppink.applicationservice.api.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.toppink.applicationservice.api.enums.ApplicationStatus;
import ru.toppink.common.kafka.OrdinalEvent;
import ru.toppink.common.kafka.OrdinalId;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApplicationChangeStatusEvent implements OrdinalEvent {

    @OrdinalId
    private UUID applicationId;
    private ApplicationStatus status;
    private String operator;
    private String client;
    private String executor;
}
