package ru.toppink.applicationservice.api.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.toppink.applicationservice.api.dto.CreatedApplicationDto;
import ru.toppink.applicationservice.api.enums.ApplicationStatus;
import ru.toppink.applicationservice.api.swagger.ApplicationSwagger;
import java.util.UUID;

@FeignClient(
        name = "application-client",
        url = "${feign.services.applications.url:https://application.toppink.ru}",
        primary = false
)
public interface ApplicationClient extends ApplicationSwagger {

}
