package ru.toppink.applicationservice.api.swagger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.toppink.applicationservice.api.dto.ApplicationDto;
import ru.toppink.applicationservice.api.dto.CreatedApplicationDto;
import ru.toppink.applicationservice.api.enums.ApplicationStatus;
import java.util.UUID;

@RequestMapping("/api/v1")
public interface ApplicationSwagger {

    @PostMapping("/create")
    void create(@RequestBody CreatedApplicationDto dto);

    @PostMapping("/start")
    void executorStartWork(@RequestParam UUID applicationId);

    @PostMapping("/delegate")
    void delegate(@RequestParam UUID applicationId,
                  @RequestParam String comment,
                  @RequestParam ApplicationStatus delegateStatus);

    @PostMapping("/complete")
    void completeWorkWithApplication(@RequestParam UUID applicationId,
                                     @RequestParam String comment);

    @PostMapping("/close")
    void closeApplication(@RequestParam UUID applicationId, String comment);

    @GetMapping("/list")
    Page<ApplicationDto> findList(Pageable pageable);
}
