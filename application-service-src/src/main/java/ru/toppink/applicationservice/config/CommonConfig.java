package ru.toppink.applicationservice.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import ru.toppink.common.securities.dao.User;
import ru.toppink.common.securities.UserFactory;

@Configuration
@RequiredArgsConstructor
public class CommonConfig {
    
    @Bean
    @Primary
    public UserFactory userFactory() {
        return login -> {
            User user = new User();
            user.setLogin(login);
            return user;
        };
    }
}
