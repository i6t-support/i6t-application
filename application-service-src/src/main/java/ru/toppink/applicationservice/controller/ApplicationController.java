package ru.toppink.applicationservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;
import ru.toppink.applicationservice.api.dto.ApplicationDto;
import ru.toppink.applicationservice.api.dto.CreatedApplicationDto;
import ru.toppink.applicationservice.api.enums.ApplicationStatus;
import ru.toppink.applicationservice.api.swagger.ApplicationSwagger;
import ru.toppink.applicationservice.service.ApplicationService;
import ru.toppink.common.securities.SecurityService;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class ApplicationController implements ApplicationSwagger {

    private final SecurityService securityService;
    private final ApplicationService applicationService;

    @Override
    @PreAuthorize("isAuthenticated() and hasRole('TUZ')")
    public void create(CreatedApplicationDto dto) {
        applicationService.createApplication(dto);
    }

    @Override
    @PreAuthorize("isAuthenticated() and hasAnyRole('OPERATOR', 'TECH_SPEC', 'SUPPORT')")
    public void executorStartWork(UUID applicationId) {
        applicationService.executorStartWork(applicationId, securityService.getLogin());
    }

    @Override
    @PreAuthorize("isAuthenticated() and hasAnyRole('OPERATOR', 'TECH_SPEC', 'SUPPORT')")
    public void delegate(UUID applicationId, String comment, ApplicationStatus delegateStatus) {
        applicationService.delegateTask(applicationId, securityService.getLogin(), delegateStatus, comment);
    }

    @Override
    @PreAuthorize("isAuthenticated() and hasAnyRole('OPERATOR', 'TECH_SPEC', 'SUPPORT')")
    public void completeWorkWithApplication(UUID applicationId, String comment) {
        applicationService.completeWorkWithApplication(applicationId, securityService.getLogin(), comment);
    }

    @Override
    @PreAuthorize("isAuthenticated() and hasRole('OPERATOR')")
    public void closeApplication(UUID applicationId, String comment) {
        applicationService.closeApplication(applicationId, securityService.getLogin(), comment);
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    public Page<ApplicationDto> findList(Pageable pageable) {
        return applicationService.findApplication(securityService.getLogin(), pageable);
    }
}
