package ru.toppink.applicationservice.kafka;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.PayloadApplicationEvent;
import org.springframework.stereotype.Service;
import ru.toppink.applicationservice.service.ApplicationService;
import ru.toppink.dispatcherservice.api.event.AssignApplicationEvent;
import ru.toppink.userservice.api.event.CreateApplicationEvent;

@Slf4j
@Service
@RequiredArgsConstructor
public class AssignApplicationListener implements ApplicationListener<PayloadApplicationEvent<AssignApplicationEvent>> {

    private final ApplicationService applicationService;

    @Override
    public void onApplicationEvent(PayloadApplicationEvent<AssignApplicationEvent> event) {
        AssignApplicationEvent payload = event.getPayload();
        applicationService.appointApplication(payload.getApplicationId(), payload.getExecutor());
    }
}
