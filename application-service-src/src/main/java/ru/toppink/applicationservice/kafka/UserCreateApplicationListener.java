package ru.toppink.applicationservice.kafka;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.PayloadApplicationEvent;
import org.springframework.stereotype.Service;
import ru.toppink.applicationservice.service.ApplicationService;
import ru.toppink.userservice.api.event.CreateApplicationEvent;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserCreateApplicationListener implements ApplicationListener<PayloadApplicationEvent<CreateApplicationEvent>> {

    private final ApplicationService applicationService;

    @Override
    public void onApplicationEvent(PayloadApplicationEvent<CreateApplicationEvent> event) {
        CreateApplicationEvent payload = event.getPayload();
        applicationService.createApplication(payload.getCreatedApplicationDto());
    }
}
