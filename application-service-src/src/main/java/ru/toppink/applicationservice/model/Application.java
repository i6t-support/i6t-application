package ru.toppink.applicationservice.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import ru.toppink.applicationservice.api.enums.ApplicationReason;
import ru.toppink.applicationservice.api.enums.ApplicationStatus;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "application")
public class Application {

    @Id
    @GeneratedValue
    private UUID id;
    private String loginClient;
    private String lastNameClient;
    private String firstNameClient;
    private String patronymicClient;
    private String phoneNumber;
    private String contractNumber;
    private OffsetDateTime createdAt;
    private OffsetDateTime updatedAt;
    private String address;
    private String appealText;
    @Enumerated(EnumType.STRING)
    private ApplicationStatus applicationStatus;
    private String operator;
    @OneToMany(mappedBy = "application", fetch = FetchType.EAGER)
    @Cascade(CascadeType.ALL)
    private List<Comment> comments;
    private String executor;

    @Enumerated(EnumType.STRING)
    private ApplicationReason reason;
}
