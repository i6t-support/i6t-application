package ru.toppink.applicationservice.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Role {

    USER("ROLE_USER"),
    OPERATOR("ROLE_OPERATOR"),
    TECH_SPEC("ROLE_TECH_SPEC"),
    SUPPORT("ROLE_SUPPORT");

    @Getter
    private final String roleName;

    public static Role valueOfName(String roleName) {
        for (Role role : values()) {
            if (role.roleName.equalsIgnoreCase(roleName)) {
                return role;
            }
        }
        throw new RuntimeException("Role " + roleName + " not found");
    }
}
