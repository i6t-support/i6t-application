package ru.toppink.applicationservice.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.toppink.applicationservice.model.Application;

import java.util.UUID;


public interface ApplicationRepository extends JpaRepository<Application, UUID> {

    Page<Application> findApplicationByExecutorOrderByUpdatedAtDesc(String executor, Pageable pageable);

    Page<Application> findApplicationByLoginClientOrderByCreatedAtDesc(String executor, Pageable pageable);
}
