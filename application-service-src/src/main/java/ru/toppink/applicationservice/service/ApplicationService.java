package ru.toppink.applicationservice.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.toppink.applicationservice.api.dto.ApplicationDto;
import ru.toppink.applicationservice.api.dto.CreatedApplicationDto;
import ru.toppink.applicationservice.api.enums.ApplicationStatus;
import ru.toppink.applicationservice.model.Application;

import java.util.UUID;

public interface ApplicationService {

    /**
     * Создание заявки
     *
     * @param application входные данные для создания
     */
    void createApplication(CreatedApplicationDto application);

    /**
     * Отправляет запрос на поиск свободного оператора для
     * новых заявок и устанавливаем статус "Поиск оператора",
     * если же оператор был когда-либо уже назначен
     * для данной заявки то вызываем {@link #appointApplication(UUID, String)}
     *
     * @param application заявка
     */
    void operatorSearch(Application application);

    /**
     * Назначить на сотрудника заявку и вызывает
     * {@link ApplicationUtilsService#notifyAboutChangeStatus(Application, ApplicationStatus)}
     * для обновления статуса заявки с статуса "делегировано" на статус
     * "в ожидание"
     *
     * @param applicationId идентификатор заявки
     * @param executor исполнитель
     */
    void appointApplication(UUID applicationId, String executor);

    /**
     * Сообщает о том, что сотрудник взялся за работу с данной заявкой
     * и обновляет с помощью метода {@link ApplicationUtilsService#notifyAboutChangeStatus(Application, ApplicationStatus)}
     * с статуса "в ожидании" на статус "в работе"
     *
     * @param applicationId идентификатор заявки
     * @param executor исполнитель
     */
    void executorStartWork(UUID applicationId, String executor);

    /**
     * Сообщает о том, что сотрудник завершил работу над задаей
     * и возвращает ее оператору. При этом изменив статус на
     * "делегировано на оператора для закрытия"
     *
     * @param applicationId идентификатор заявки
     * @param executor исполнитель
     * @param comment текст комментария
     */
    void completeWorkWithApplication(UUID applicationId, String executor, String comment);

    /**
     * Делегирует задачу с одного пользователя на другого, путем
     * изменения статуса на "делегировано"
     *
     * @param applicationId идентификатор заявки
     * @param currentExecutor текущий исполнитель
     * @param delegateStatus статус делегирования
     */
    void delegateTask(UUID applicationId, String currentExecutor, ApplicationStatus delegateStatus, String comment);

    /**
     * Закрытие заявки и удаление ее из хранилища
     * (доступно только для операторов)
     *  @param applicationId идентификатор заявки
     * @param comment
     * @param executor закрывающий
     */
    void closeApplication(UUID applicationId, String comment, String executor);

    Page<ApplicationDto> findApplication(String executor, Pageable pageable);
}
