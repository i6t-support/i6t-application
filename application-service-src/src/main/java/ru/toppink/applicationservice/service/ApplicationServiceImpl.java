package ru.toppink.applicationservice.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;
import ru.toppink.applicationservice.api.dto.ApplicationDto;
import ru.toppink.applicationservice.api.dto.CommentDto;
import ru.toppink.applicationservice.api.dto.CreatedApplicationDto;
import ru.toppink.applicationservice.api.enums.ApplicationStatus;
import ru.toppink.applicationservice.model.Application;
import ru.toppink.applicationservice.model.Comment;
import ru.toppink.applicationservice.model.Role;
import ru.toppink.applicationservice.repository.ApplicationRepository;
import ru.toppink.common.exception.SimpleException;
import ru.toppink.common.securities.SecurityService;
import ru.toppink.userservice.api.dto.BriefUserDto;
import ru.toppink.userservice.api.feign.UserClient;

@Slf4j
@RequiredArgsConstructor
@Service
public class ApplicationServiceImpl implements ApplicationService {

    private static final String USER_ROLE = "ROLE_USER";

    private final UserClient userClient;
    private final EmployeeService employeeService;
    private final ApplicationUtilsService applicationUtilsService;
    private final ApplicationRepository repository;
    private final SecurityService securityService;

    private static final Map<ApplicationStatus, ApplicationStatus> STATUS_DELEGATE_TO_WAITING = Map.of(
            ApplicationStatus.OPERATOR_SEARCH, ApplicationStatus.OPERATOR_WAITING,
            ApplicationStatus.DELEGATE_TO_OPERATOR, ApplicationStatus.OPERATOR_WAITING,
            ApplicationStatus.DELEGATE_TO_SUPPORT, ApplicationStatus.SUPPORT_WAITING,
            ApplicationStatus.DELEGATE_TO_TECH_SPEC, ApplicationStatus.TECH_SPEC_WAITING,
            ApplicationStatus.DELEGATE_TO_OPERATOR_FOR_CLOSE, ApplicationStatus.OPERATOR_WAITING,
            ApplicationStatus.WAITING_APPROVE_CLIENT, ApplicationStatus.WAITING_APPROVE_CLIENT
    );

    private static final Map<ApplicationStatus, ApplicationStatus> STATUS_WAITING_TO_IN_WORK = Map.of(
            ApplicationStatus.OPERATOR_WAITING, ApplicationStatus.OPERATOR_IN_WORK,
            ApplicationStatus.SUPPORT_WAITING, ApplicationStatus.SUPPORT_IN_WORK,
            ApplicationStatus.TECH_SPEC_WAITING, ApplicationStatus.TECH_SPEC_IN_WORK
    );

    @Override
    public void createApplication(CreatedApplicationDto applicationDto) {
        log.debug("Creating application - start: {}", applicationDto);

        Application application = new Application();
        application.setLastNameClient(applicationDto.getLastNameClient());
        application.setFirstNameClient(applicationDto.getFirstNameClient());
        application.setPatronymicClient(applicationDto.getPatronymicClient());
        application.setPhoneNumber(applicationDto.getPhoneNumber());
        application.setContractNumber(applicationDto.getContractNumber());
        application.setCreatedAt(OffsetDateTime.now());
        application.setUpdatedAt(OffsetDateTime.now());
        application.setAddress(applicationDto.getAddress());
        application.setAppealText(applicationDto.getAppealText());
        application.setApplicationStatus(ApplicationStatus.CREATED);
        application.setComments(new ArrayList<>());
        application.setLoginClient(applicationDto.getLoginClient());
        application.setReason(applicationDto.getReason());
        Application saved = repository.save(application);

        operatorSearch(application);
        log.debug("Creating application - end: {}", saved);
    }

    @Override
    public void operatorSearch(Application application) {

        log.debug("Searching for operator - start: {}", application);

        application.setApplicationStatus(ApplicationStatus.OPERATOR_SEARCH);
        repository.save(application);

        applicationUtilsService.notifyAboutChangeStatus(application, ApplicationStatus.OPERATOR_SEARCH);
        employeeService.findOperator(application.getId());

        log.debug("Searching for operator - end");
    }

    @Override
    public void appointApplication(UUID applicationId, String executor) {
        log.debug("Appointing application - start: {}, {} ", applicationId, executor);

        Application application = repository.findById(applicationId)
                .orElseThrow(() -> new SimpleException("Application with id " + applicationId + " not found"));

        List<Role> roles = employeeService.getRoles(executor);

        if (roles.contains(Role.OPERATOR)) {
            application.setOperator(executor);
        }

        application.setUpdatedAt(OffsetDateTime.now());
        application.setExecutor(executor);

        ApplicationStatus oldStatus = application.getApplicationStatus();
        ApplicationStatus newStatus = STATUS_DELEGATE_TO_WAITING.get(oldStatus);

        if (newStatus == null) {
            throw new SimpleException("New status for status " + oldStatus + " not found");
        }

        application.setApplicationStatus(newStatus);
        Application saved = repository.save(application);
        applicationUtilsService.notifyAboutChangeStatus(application, newStatus);

        log.debug("Appointing application - end: {}", saved);
    }

    @Override
    public void executorStartWork(UUID applicationId, String executor) {

        log.debug("Executor is starting work - start: {}, {}", applicationId, executor);

        Application application = repository.findById(applicationId)
                .orElseThrow(() -> new SimpleException("Application with id " + applicationId + " not found"));

        String currentExecutor = application.getExecutor();
        if (!executor.equals(currentExecutor)) {
            throw new SimpleException("Application with id " + applicationId + " doesn't belong to " + executor);
        }

        ApplicationStatus oldStatus = application.getApplicationStatus();
        ApplicationStatus newStatus = STATUS_WAITING_TO_IN_WORK.get(oldStatus);

        if (newStatus == null) {
            throw new SimpleException("New status for status " + oldStatus + "not found");
        }

        application.setUpdatedAt(OffsetDateTime.now());
        application.setApplicationStatus(newStatus);
        Application saved = repository.save(application);
        applicationUtilsService.notifyAboutChangeStatus(application, newStatus);

        log.debug("Executor is starting work - end: {}", saved);
    }

    @Override
    public void completeWorkWithApplication(UUID applicationId, String executor, String commentText) {

        log.debug("Completing work with application - start: {}, {}, {}", applicationId, executor, commentText);

        Application application = repository.findById(applicationId)
                .orElseThrow(() -> new SimpleException("Application with id" + applicationId + " not found"));

        String currentExecutor = application.getExecutor();

        if (!executor.equals(currentExecutor)) {
            throw new SimpleException("Application with id " + applicationId + " doesn't belong to " + executor);
        }

        Comment comment = applicationUtilsService.createCommentForApplication(
                application, executor, commentText, false);
        List<Comment> comments = application.getComments();

        comments.add(comment);
        application.setComments(comments);

        ApplicationStatus newStatus = ApplicationStatus.DELEGATE_TO_OPERATOR_FOR_CLOSE;

        application.setUpdatedAt(OffsetDateTime.now());
        application.setApplicationStatus(newStatus);
        Application saved = repository.save(application);
        applicationUtilsService.notifyAboutChangeStatus(application, newStatus);

        log.debug("Completing work with application - end: {}", saved);
    }

    @Override
    public void delegateTask(UUID applicationId, String executor, ApplicationStatus delegateStatus, String commentText) {

        log.debug("Delegating task - start: {}, {}, {}, {}", applicationId, executor, delegateStatus, commentText);

        Application application = repository.findById(applicationId)
                .orElseThrow(() -> new SimpleException("Application with id" + applicationId + " not found"));

        String currentExecutor = application.getExecutor();

        if (!executor.equals(currentExecutor)) {
            throw new SimpleException("Application with id " + applicationId + " doesn't belong to " + executor);
        }

        Comment comment = applicationUtilsService.createCommentForApplication(
                application, executor, commentText, false);
        List<Comment> comments = application.getComments();

        comments.add(comment);
        application.setComments(comments);

        application.setUpdatedAt(OffsetDateTime.now());
        application.setApplicationStatus(delegateStatus);
        Application saved = repository.save(application);
        applicationUtilsService.notifyAboutChangeStatus(application, delegateStatus);

        log.debug("Delegating status - end: {}", saved);
    }

    @Override
    public void closeApplication(UUID applicationId, String executor, String commentText) {
        log.debug("Close application - start: applicationId = {}, executor = {}, comment = {}",
                applicationId, executor, commentText);

        Application application = repository.findById(applicationId)
                .orElseThrow(() -> new SimpleException("Application with id" + applicationId + " not found"));

        String currentExecutor = application.getExecutor();

        if (!executor.equals(currentExecutor)) {
            throw new SimpleException("Application with id " + applicationId + " doesn't belong to " + executor);
        }

        final ApplicationStatus status = ApplicationStatus.WAITING_APPROVE_CLIENT;

        Comment comment = applicationUtilsService
                .createCommentForApplication(application, executor, commentText, true);
        List<Comment> comments = application.getComments();
        comments.add(comment);

        application.setApplicationStatus(status);
        application.setExecutor(null);

        repository.save(application);

        applicationUtilsService.notifyAboutChangeStatus(application, status);

        log.debug("Close application - end");
    }

    @Override
    public Page<ApplicationDto> findApplication(String login, Pageable pageable) {
        log.debug("Finding applications by executor - start: executor={}, pageable={}", login, pageable);

        Set<String> roles = securityService.getRoles();
        Page<Application> applicationPage = roles.contains(USER_ROLE)
                ? repository.findApplicationByLoginClientOrderByCreatedAtDesc(login, pageable)
                : repository.findApplicationByExecutorOrderByUpdatedAtDesc(login, pageable);

        List<ApplicationDto> content = applicationPage.stream().map(this::convert).collect(Collectors.toList());
        Page<ApplicationDto> applicationDtos = new PageImpl<>(
                content, applicationPage.getPageable(), applicationPage.getTotalElements());

        log.debug("Finding applications by executor - end: {}", applicationDtos);
        return applicationDtos;
    }

    private ApplicationDto convert(Application application) {
        boolean isUser = securityService.getRoles().contains(Role.USER.getRoleName());

        List<CommentDto> comments;
        if (!CollectionUtils.isEmpty(application.getComments())) {
            comments = application.getComments().stream()
                    .filter(comment -> !isUser || comment.isClientCanRead())
                    .map(comment -> CommentDto.builder()
                    .id(comment.getId())
                    .text(comment.getText())
                    .createdAt(comment.getCreatedAt())
                    .author(loadUser(comment.getAuthor()))
                    .build()
            )
                    .collect(Collectors.toList());
        } else {
            comments = new ArrayList<>();
        }

        return ApplicationDto.builder()
                .id(application.getId())
                .lastNameClient(application.getLastNameClient())
                .firstNameClient(application.getFirstNameClient())
                .patronymicClient(application.getPatronymicClient())
                .phoneNumber(application.getPhoneNumber())
                .createdAt(application.getCreatedAt())
                .updatedAt(application.getUpdatedAt())
                .address(application.getAddress())
                .appealText(application.getAppealText())
                .applicationStatus(application.getApplicationStatus())
                .operator(application.getOperator())
                .contractNumber(application.getContractNumber())
                .comments(comments)
                .executor(application.getExecutor())
                .reason(application.getReason())
                .build();

    }

    private BriefUserDto loadUser(String login) {
        if (login == null) {
            return null;
        }

        return userClient.getUserProfile(login);
    }
}
