package ru.toppink.applicationservice.service;

import ru.toppink.applicationservice.api.enums.ApplicationStatus;
import ru.toppink.applicationservice.model.Application;
import ru.toppink.applicationservice.model.Comment;

public interface ApplicationUtilsService {

    /**
     * Создает комментарий и прикладывает его к заявке
     *
     * @param application заявка
     * @param author логин автора комментарий
     * @param text текст комментария
     * @param canReadClient сможет ли прочесть сообщение клиент
     */
    Comment createCommentForApplication(Application application, String author, String text, boolean canReadClient);

    /**
     * Изменяет статус заявки
     * Нужно обновить updateAt
     *
     * @param application идентификатор заявки
     * @param status новый статус заявки
     */
    void notifyAboutChangeStatus(Application application, ApplicationStatus status);
}
