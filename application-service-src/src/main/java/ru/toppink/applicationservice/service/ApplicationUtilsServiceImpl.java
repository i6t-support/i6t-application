package ru.toppink.applicationservice.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.toppink.applicationservice.api.enums.ApplicationStatus;
import ru.toppink.applicationservice.api.event.ApplicationChangeStatusEvent;
import ru.toppink.applicationservice.model.Application;
import ru.toppink.applicationservice.model.Comment;
import ru.toppink.common.kafka.KafkaSender;
import java.time.OffsetDateTime;

@Slf4j
@RequiredArgsConstructor
@Service
public class ApplicationUtilsServiceImpl implements ApplicationUtilsService {

    private final KafkaSender kafkaSender;

    @Override
    public Comment createCommentForApplication(Application application, String author, String text, boolean canReadClient) {
        log.debug("Creating a comment - start: {}, {}, {}", author, text, canReadClient);

        Comment comment = new Comment();
        comment.setAuthor(author);
        comment.setApplication(application);
        comment.setText(text);
        comment.setCreatedAt(OffsetDateTime.now());
        comment.setClientCanRead(canReadClient);

        return comment;
    }

    @Override
    public void notifyAboutChangeStatus(Application application, ApplicationStatus status) {
        log.debug("Notify about change status - start: application = {}, status = {}", application, status);

        ApplicationChangeStatusEvent build = ApplicationChangeStatusEvent.builder()
                .applicationId(application.getId())
                .operator(application.getOperator())
                .client(application.getLoginClient())
                .executor(application.getExecutor())
                .status(status)
                .build();

        kafkaSender.sendToApplicationOut(build);
    }
}
