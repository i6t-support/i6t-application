package ru.toppink.applicationservice.service;

import ru.toppink.applicationservice.model.Role;
import java.util.List;
import java.util.UUID;

public interface EmployeeService {

    /**
     * Отправляет запрос на поиск оператора для заявки
     *
     * @param applicationId индентификатор заявки
     */
    void findOperator(UUID applicationId);

    /**
     * Возвращает список ролей для пользователя
     *
     * @param login искомый пользователь
     *
     * @return Список из:
     * ROLE_USER
     * ROLE_OPERATOR
     * ROLE_TECH_SPEC
     * ROLE_SUPPORT
     * и подобные
     */
    List<Role> getRoles(String login);
}
