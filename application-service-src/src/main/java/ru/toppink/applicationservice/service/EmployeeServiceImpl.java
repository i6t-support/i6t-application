package ru.toppink.applicationservice.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.toppink.applicationservice.model.Role;
import ru.toppink.applicationservice.repository.ApplicationRepository;
import ru.toppink.common.securities.SecurityService;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final ApplicationRepository repository;
    private final SecurityService securityService;

    @Override
    public void findOperator(UUID applicationId) {

    }

    @Override
    public List<Role> getRoles(String login) {
        return new ArrayList<>();
    }
}
