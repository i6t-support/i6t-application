create table application
(
	id                 uuid        not null,
	login_client       varchar     not null,
	last_name_client   varchar     not null,
	first_name_client  varchar     not null,
	patronymic_client  varchar,
	phone_number       varchar     not null,
	contract_number    varchar     not null,
	created_at         timestamptz not null,
	updated_at         timestamptz not null,
	address            varchar     not null,
	appeal_text        varchar     not null,
	application_status varchar     not null,
	operator           varchar,
	executor           varchar,
	primary key (id)
);

create table comment
(
	id              uuid        not null,
	text            varchar     not null,
	created_at      timestamptz not null,
	client_can_read boolean     not null,
	application_id  uuid        not null,
	primary key (id)
);

alter table comment
	add constraint application_comment_fk
		foreign key (application_id)
			references application;